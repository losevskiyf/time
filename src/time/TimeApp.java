package time;

import java.util.Scanner;

public class TimeApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int day, month, year;
        while (true) {
            System.out.println("Enter date in format 'DD.MM.YYYY'");
            String date = scanner.nextLine();
            String[] dayMonthYear;
            if (date.matches("[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]")) {

                dayMonthYear = date.split("\\.");
                day = Integer.parseInt(dayMonthYear[0]);
                month = Integer.parseInt(dayMonthYear[1]);
                year = Integer.parseInt(dayMonthYear[2]);

                if(month>12
                        || month==1&&day>31
                        || month==2&&day>29&&year%4==0 || month==2&&day>28&&year%4!=0
                        || month==3&&day>31
                        || month==4&&day>30
                        || month==5&&day>31
                        || month==6&&day>30
                        || month==7&&day>31
                        || month==8&&day>31
                        || month==9&&day>30
                        || month==10&&day>31
                        || month==11&&day>30
                        || month==12&&day>31)
                {
                    System.out.println("Incorrect input. Try again");
                }
                else break;
            }else {
                System.out.println("Incorrect input. Try again");
            }
        }
        if(year%4==0) System.out.print("Год високосный");
        else System.out.print("Год невисокосный");
    }
}